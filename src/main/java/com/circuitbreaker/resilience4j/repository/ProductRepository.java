package com.circuitbreaker.resilience4j.repository;

import com.circuitbreaker.resilience4j.model.Product;
import com.circuitbreaker.resilience4j.services.delays.IDelay;
import com.circuitbreaker.resilience4j.services.delays.NoDelay;
import com.circuitbreaker.resilience4j.services.failures.IFailure;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ProductRepository {
	private List<Product> products;
	IFailure potentialFailure;
	IDelay potentialDelay = new NoDelay();
	int count;

	public ProductRepository(IFailure potentialFailure) {
		this.potentialFailure = potentialFailure;
        products = Arrays.asList(
                new Product(1, "iPhone", 300),
                new Product(2, "Android", 300),
                new Product(3, "XBOX ONE", 400),
                new Product(4, "PS5", 400)
        );
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}
	
	public List<Product> findByQuantity(int qty) {
		potentialDelay.delay();
		potentialFailure.fail();
		count++;
        List<Product> result = new ArrayList<Product>();
		for(Product p : this.products) {
        	if(p.getQuantity() == qty) {
        		result.add(p);
        	}
        }
		System.out.println("request " + count);
		System.out.println("Product search successful");
		return result;
	}

	// these methods will be used to set potential failure and delay to test circuit breaker with various configurations.
	public void setPotentialFailure(IFailure potentialFailure) {
		this.potentialFailure = potentialFailure;
	}

	public void setPotentialDelay(IDelay potentialDelay) {
		this.potentialDelay = potentialDelay;
	}
}
