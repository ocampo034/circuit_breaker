package com.circuitbreaker.resilience4j.services;


import com.circuitbreaker.resilience4j.services.failures.NoFailure;
import com.circuitbreaker.resilience4j.model.Product;
import com.circuitbreaker.resilience4j.repository.ProductRepository;
import com.circuitbreaker.resilience4j.services.delays.NSecondsDelay;
import com.circuitbreaker.resilience4j.services.failures.SucceedNTimesThenFail;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.circuitbreaker.CircuitBreakerConfig;
import io.github.resilience4j.circuitbreaker.CircuitBreakerRegistry;
import io.vavr.API;
import io.vavr.control.Try;
import lombok.extern.slf4j.Slf4j;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.function.Supplier;

import static io.vavr.API.*;
import static io.vavr.Predicates.*;


@Slf4j
public class ProductSearchService {
	// By default, we are assuming there will be no failure and no delay involved when we search for products.

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss SSS");
    CircuitBreaker circuitBreaker;
    ProductRepository repo = new ProductRepository(new SucceedNTimesThenFail(0));

    public ProductSearchService() {


        CircuitBreakerConfig config = CircuitBreakerConfig
                .custom()
                .slidingWindowType(CircuitBreakerConfig.SlidingWindowType.COUNT_BASED)
                .slidingWindowSize(5)
                .slowCallRateThreshold(70.0f)
                .failureRateThreshold(70.0f)
                .slowCallDurationThreshold(Duration.ofSeconds(2))
                .automaticTransitionFromOpenToHalfOpenEnabled(true)
                .writableStackTraceEnabled(true)
                .permittedNumberOfCallsInHalfOpenState(3)
                //.maxWaitDurationInHalfOpenState(Duration.ofSeconds(10))
                .waitDurationInOpenState(Duration.ofSeconds(20))
                .build();

        CircuitBreakerRegistry registry = CircuitBreakerRegistry.of(config);
        circuitBreaker = registry.circuitBreaker("productSearchService2");

        //repo.setPotentialDelay(new NSecondsDelay(3));
        configEventPublisher(circuitBreaker);

    }

    public Try<List<Product>> searchProducts(int qty, int fail) {
        System.out.println("Searching for products; current time = " + LocalDateTime.now().format(formatter));

        repo.setPotentialFailure(new SucceedNTimesThenFail(fail));
        repo.setPotentialDelay(new NSecondsDelay(0));
        System.out.println("Estado" + circuitBreaker.getState());
        Supplier<List<Product>> decoratedSupplier = CircuitBreaker
                .decorateSupplier(circuitBreaker, () -> repo.findByQuantity(qty));

        Try<List<Product>> result = Try.ofSupplier(decoratedSupplier);

        return result;
    }

    public boolean estadoCircuito() {
        return Match(circuitBreaker.getState()).of(
                Case($(CircuitBreaker.State.OPEN), false),
                Case($(isIn(CircuitBreaker.State.HALF_OPEN, CircuitBreaker.State.CLOSED)), true),
                Case($(), false));
    }

    public Try<List<Product>> searchProducts2(int qty, int delay) {
        System.out.println("Searching for products; current time = " + LocalDateTime.now().format(formatter));
        repo.setPotentialFailure(new NoFailure());
        repo.setPotentialDelay(new NSecondsDelay(delay));

        Supplier<List<Product>> decoratedSupplier = CircuitBreaker
                .decorateSupplier(circuitBreaker, () -> repo.findByQuantity(qty));

        Try<List<Product>> result = Try.ofSupplier(decoratedSupplier);

        return result;
    }


    private void configEventPublisher(CircuitBreaker circuitBreaker) {
        circuitBreaker.getEventPublisher()
                .onFailureRateExceeded(event -> log.error("circuit breaker {} failure rate {} on {}",
                        event.getCircuitBreakerName(), event.getFailureRate(), event.getCreationTime())
                )
                .onSlowCallRateExceeded(event -> {
                        log.error("circuit breaker {} slow call rate {} on {}",
                                    event.getCircuitBreakerName(), event.getSlowCallRate(), event.getCreationTime());
                        log.info("----------- ENVIAR USUARIO A COLA DE ESPERA ---------------");
                    }
                )
                .onCallNotPermitted(event -> log.error("circuit breaker {} call not permitted {}",
                        event.getCircuitBreakerName(), event.getCreationTime())
                )
                .onError(event -> log.error("circuit breaker {} error with duration {}s",
                        event.getCircuitBreakerName(), event.getElapsedDuration().getSeconds())
                )
                .onStateTransition(
                        event ->
                        {
                            log.warn("circuit breaker {} state transition from {} to {} on {}",
                                    event.getCircuitBreakerName(), event.getStateTransition().getFromState(),
                                    event.getStateTransition().getToState(), event.getCreationTime());

                            if(event.getStateTransition().getToState().equals(CircuitBreaker.State.OPEN)){
                                repo = new ProductRepository(new SucceedNTimesThenFail(10));
                                //repo.setPotentialDelay(new NSecondsDelay(1));
                            }else {
                                if(event.getStateTransition().getToState().equals(CircuitBreaker.State.CLOSED)){
                                    repo = new ProductRepository(new SucceedNTimesThenFail(20));
                                    log.info("-----------  INFORMAR A USUARIOS  ----------------");
                                    //repo.setPotentialDelay(new NSecondsDelay(1));
                                }
                            }


                        }
                );
    }

}