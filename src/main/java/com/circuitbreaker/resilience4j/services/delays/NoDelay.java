package com.circuitbreaker.resilience4j.services.delays;

public class NoDelay implements IDelay {
  @Override
  public void delay() {
  }
}
