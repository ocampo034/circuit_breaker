package com.circuitbreaker.resilience4j.services.delays;

public interface IDelay {
  void delay();
}
