package com.circuitbreaker.resilience4j.services.failures;

import com.circuitbreaker.resilience4j.exceptions.ProductServiceException;

public class SucceedNTimesThenFail implements IFailure {

  int n;
  int successCount;

  public SucceedNTimesThenFail(int n) {
    this.n = n;
  }


  @Override
  public void fail() {
    if (n == 0) {
      successCount++;
      return;
    }
    System.out.println("Error occurred during product search");
    throw new ProductServiceException("Error occurred during product search");
  }
}
