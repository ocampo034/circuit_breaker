package com.circuitbreaker.resilience4j.services.failures;

public class NoFailure implements IFailure {
    @Override
    public void fail() {
    }
}
