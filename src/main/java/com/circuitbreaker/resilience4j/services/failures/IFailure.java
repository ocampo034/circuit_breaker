package com.circuitbreaker.resilience4j.services.failures;

public interface IFailure {
    void fail();
}