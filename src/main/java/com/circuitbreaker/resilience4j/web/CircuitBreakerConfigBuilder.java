package com.circuitbreaker.resilience4j.web;

import io.github.resilience4j.circuitbreaker.CircuitBreakerConfig;

import java.time.Duration;

public class CircuitBreakerConfigBuilder {

    public static CircuitBreakerConfig config() {
        CircuitBreakerConfig config = CircuitBreakerConfig
                .custom()
                .slidingWindowType(CircuitBreakerConfig.SlidingWindowType.TIME_BASED)
                .minimumNumberOfCalls(10)
                .slidingWindowSize(10)
                .slowCallRateThreshold(70.0f)
                .slowCallDurationThreshold(Duration.ofSeconds(1))
                .writableStackTraceEnabled(false)
                .build();

        return  config;
    }
}
