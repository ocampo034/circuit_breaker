package com.circuitbreaker.resilience4j.web;

import com.circuitbreaker.resilience4j.model.Product;
import com.circuitbreaker.resilience4j.model.Response;
import java.util.Collections;
import java.util.List;
import com.circuitbreaker.resilience4j.services.ProductSearchService;
import io.github.resilience4j.circuitbreaker.CallNotPermittedException;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import static io.vavr.API.*;
import static io.vavr.Patterns.$Failure;
import static io.vavr.Patterns.$Success;
import static io.vavr.Predicates.instanceOf;


@Slf4j
@RestController
@RequestMapping(value = "/api")
public class Resilience4jController {

  ProductSearchService service = new ProductSearchService();


  @GetMapping(value = "/my_circuit_breaker_implemntation_fail_noDelay/{fail}", produces = MediaType.APPLICATION_JSON_VALUE)
  public Mono<Response<List<Product>>> my_circuit_breaker_implemntation_fail_noDelay(@PathVariable int fail) {

      Response<List<Product>> res = io.vavr.API.Match(service.searchProducts(300, fail)).of(
              Case($Success($()), value -> toResponse(HttpStatus.OK, value)),
              Case($Failure($()), exception ->
                      io.vavr.API.Match(exception).of(
                      Case($(instanceOf(CallNotPermittedException.class)),
                        () -> {
                            log.info("Guardar usuario para notificacion posterior");
                            return toResponse(HttpStatus.GATEWAY_TIMEOUT, Collections.emptyList());
                        }
                      ),
                      Case($(), toResponse(HttpStatus.INTERNAL_SERVER_ERROR, Collections.emptyList()))))
          );

    return Mono.just(res);
  }

    @GetMapping(value = "/my_circuit_breaker_implemntation_delay/{delay}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Response<List<Product>>> my_circuit_breaker_implemntation_delay(@PathVariable int delay) {

        Response<List<Product>> res = io.vavr.API.Match(service.searchProducts2(300, delay)).of(
                Case($Success($()), value -> toResponse(HttpStatus.OK, value)),
                Case($Failure($()), exception ->
                        io.vavr.API.Match(exception).of(
                                Case($(instanceOf(CallNotPermittedException.class)),
                                        () -> {
                                            log.info("Guardar usuario para notificacion posterior");
                                            return toResponse(HttpStatus.GATEWAY_TIMEOUT, Collections.emptyList());
                                        }
                                ),
                                Case($(), toResponse(HttpStatus.INTERNAL_SERVER_ERROR, Collections.emptyList()))))
        );

        return Mono.just(res);
    }

    @GetMapping(value = "/my_circuit_breaker_state")
    public Boolean my_circuit_breaker_state() {
      return new Boolean(service.estadoCircuito());
    }

  private Mono<Response<List<Product>>> toOkResponse(List<Product> productsSupplier) {
    return Mono.just(toResponse(HttpStatus.OK, productsSupplier));
  }

  private Response<List<Product>> toResponse(HttpStatus httpStatus, List<Product> productsSupplier) {
    return Response.<List<Product>>builder()
        .code(httpStatus.value())
        .status(httpStatus.getReasonPhrase())
        .data(productsSupplier)
        .build();
  }

}
